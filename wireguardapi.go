package main

import (
    "bufio"
    "crypto/sha1"
    "flag"
    "fmt"
    "encoding/json"
    "net/http"
    "log"
    "os"
    "os/exec"
    "strings"
)

func handler(w http.ResponseWriter, r *http.Request) {
    peers, err := peersDump()
    if err != nil {
        fmt.Fprintf(w, err.Error())
        return
    }

    res, err := json.Marshal(peers)
    if err != nil {
        fmt.Fprintf(w, err.Error())
        return
    }

    fmt.Fprintln(w, string(res))
}

func peersDump() (map[string]map[string]string, error){
    peers := make(map[string]map[string]string)

    cmd := exec.Command(*wgBin, "show", "all", "dump")
    stdout, err := cmd.Output()
    if err != nil {
        return peers, err
    }

    var parts []string
    scanner := bufio.NewScanner(strings.NewReader(string(stdout)))
    for scanner.Scan() {
        parts = strings.Split(scanner.Text(), "\t")
        if len(parts) < 7 {
            continue
        }

        name, err := peerName(*wgLib, parts[1])
        if err != nil {
            log.Println(err.Error())
            continue
        }

        peers[name] = map[string]string{
            "endpoint": parts[3],
            "latest_handshake": parts[5],
            "transfer_rx": parts[6],
            "transfer_tx": parts[7],
        }
    }

    return peers, nil
}

func peerName(path, publicKey string) (string, error) {
    hasher := sha1.New()
    hasher.Reset()
    _, err := hasher.Write([]byte(fmt.Sprintf("%s\n", publicKey)))
    if err != nil {
        return "", err
    }

    sha1 := hasher.Sum([]byte(nil))
    filePath := fmt.Sprintf("%s/%s", path, fmt.Sprintf("%x", sha1))

    _, err = os.Stat(filePath)
    if err != nil {
        return "", err
    }

    data, err := os.ReadFile(filePath)
    if err != nil {
        return "", err
    }

    return strings.Trim(string(data), "\n"), nil
}

var (
    addr, wgLib, wgBin *string
)

func main() {
    addr = flag.String("addr", ":80", "API server address and port")
    wgLib = flag.String("wg_path", "/var/lib/wireguard/", "Path to wireguard lib")
    wgBin = flag.String("wg_bin", "/usr/bin/wg", "Wireguard binary")
    flag.Parse()

    http.HandleFunc("/", handler)
    log.Fatal(http.ListenAndServe(*addr, nil))
}
